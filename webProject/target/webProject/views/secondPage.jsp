<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />
</head>
<body>
	<h1>Hey you this is a list page</h1>
	<br>

	<div class="col-md-4">
		<table class="table">
			<tr>
				<th>Name</th>
				<th>Age</th>
			</tr>
			<c:forEach items="${listSons}" var="item">
				<tr>
					<td>${item.name}</td>
					<td>${item.age}</td>
				</tr>
			</c:forEach>
		</table>
	</div>

</body>
</html>