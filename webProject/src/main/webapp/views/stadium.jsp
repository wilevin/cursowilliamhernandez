<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<title>STADIUM</title>


</head>
<body>



	<div class="alert-danger">${error}</div>

	<form action="stadium" method="post" class="form-horizontal" style="margin:0 auto">

		<div class="form-group">
			<label for="txt-first"> ENTER FOLIO </label> <input
				id="txt-folio" class="form-control" type="text" name="folio" 
				placeholder="Enter Folio">
		</div>
		
		<button type="submit" class="btn btn-primary">Registrar</button>
	</form>
	<br>
<div class="col-md-5 ">
		<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">

			<thead>
				<tr>

					<th scope="col">Folio</th>
				</tr>
			</thead>
			<c:forEach items="${lista}" var="item">
				<tr>
					<td>${item}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<script type="text/javascript">
	
	$(document).ready(function () {
		
		$('#datatable').DataTable();
		
		
	});
	</script>
</body>
</html>