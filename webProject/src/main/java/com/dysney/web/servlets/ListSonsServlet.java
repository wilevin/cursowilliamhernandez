package com.dysney.web.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.beans.FirtsSon;

@WebServlet("/listSons")
public class ListSonsServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2282418199490233484L;
	List<FirtsSon> listSons = new ArrayList<FirtsSon>();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		request.setAttribute("listSons", listSons);
		
		request.getRequestDispatcher("/views/ListSons.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = (String)request.getParameter("name");
		int age = Integer.parseInt(request.getParameter("age"));
		
		listSons.add(new FirtsSon(name, age));
		request.setAttribute("listSons", listSons);
		request.getRequestDispatcher("/views/ListSons.jsp").forward(request, response);
		
	}
}
