package com.dysney.web.servlets.exception;

public class StadiumLimitException extends Exception{

	private static final long serialVersionUID = 1L;

	
	public StadiumLimitException (){
		super();
	}
	
	public StadiumLimitException (String msg){
		super(msg);
	}
	
	
	public StadiumLimitException (Throwable throwable){
		super(throwable);
	}
	
	public StadiumLimitException (String msg, Throwable throwable){
		super(msg,throwable);
	}
	
}
