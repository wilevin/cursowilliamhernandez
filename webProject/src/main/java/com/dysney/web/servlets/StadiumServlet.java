package com.dysney.web.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dysney.web.servlets.exception.NoUserSessionException;
import com.dysney.web.servlets.exception.StadiumLimitException;
import com.dysney.web.servlets.exception.StoleException;
import com.dysney.web.servlets.utils.ServletUtils;

@WebServlet("/stadium")
public class StadiumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ArrayList<String> lista = new ArrayList<String>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			request.getRequestDispatcher("/views/stadium.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		String folio = (String) request.getParameter("folio");
		try {

			ServletUtils.validateFolio(lista, folio);
			ServletUtils.validateLimitPeople(lista);

			lista.add(folio);
		} catch (StadiumLimitException e) {
			request.setAttribute("error", "Stadium is out");
			e.printStackTrace();
		}catch (StoleException fse) {
			request.setAttribute("error", "Fucking Stole");
			fse.printStackTrace();
		}
		request.setAttribute("lista", lista);
		request.getRequestDispatcher("/views/stadium.jsp").forward(request, response);
	}

}
