package com.dysney.web.servlets.exception;

public class StoleException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	
	public StoleException (){
		super();
	}
	
	public StoleException (String msg){
		super(msg);
	}
	
	
	public StoleException (Throwable throwable){
		super(throwable);
	}
	
	public StoleException (String msg, Throwable throwable){
		super(msg,throwable);
	}
	
}
