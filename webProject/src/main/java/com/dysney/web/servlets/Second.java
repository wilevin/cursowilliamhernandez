package com.dysney.web.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.beans.FirtsSon;

@WebServlet("/secondPage")
public class Second extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2282418199490233484L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<FirtsSon> listSons = new ArrayList<FirtsSon>();
		listSons.add(new FirtsSon("Juan", 28));
		listSons.add(new FirtsSon("Chava", 24));
		listSons.add(new FirtsSon("Bloda", 22));
		
		request.setAttribute("listSons", listSons);
		
		request.getRequestDispatcher("/views/secondPage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}
}
