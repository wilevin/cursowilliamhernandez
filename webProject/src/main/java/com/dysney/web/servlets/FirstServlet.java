package com.dysney.web.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FirstServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/operation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		double firstNumber = Double.parseDouble(request.getParameter("firstNumber"));
		double secondNumber = Double.parseDouble(request.getParameter("secondNumber"));
		
		double result = firstNumber * secondNumber;
		
		request.setAttribute("result", result);
		
		request.getRequestDispatcher("/operation.jsp").forward(request, response);;
	}

}
