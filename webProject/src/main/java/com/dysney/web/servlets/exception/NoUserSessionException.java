package com.dysney.web.servlets.exception;

public class NoUserSessionException extends Exception{

	private static final long serialVersionUID = 1L;

	
	public NoUserSessionException (){
		super();
	}
	
	public NoUserSessionException (String msg){
		super(msg);
	}
	
	
	public NoUserSessionException (Throwable throwable){
		super(throwable);
	}
	
	public NoUserSessionException (String msg, Throwable throwable){
		super(msg,throwable);
	}
	
}
