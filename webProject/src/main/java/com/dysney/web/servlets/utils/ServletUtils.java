package com.dysney.web.servlets.utils;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dysney.web.servlets.exception.NoUserSessionException;
import com.dysney.web.servlets.exception.StadiumLimitException;
import com.dysney.web.servlets.exception.StoleException;

public class ServletUtils {

	public static final String KEY_USER = "usuario";
	public static final int MAX_STADIUM = 5;

	public static void validateSession(HttpServletRequest request) throws NoUserSessionException {

		HttpSession session = request.getSession();

		Object userObject = session.getAttribute(KEY_USER);

		if (userObject == null) {
			throw new NoUserSessionException("you are not session");
		}

	}

	public static void validateLimitPeople(ArrayList<String> lista) throws StadiumLimitException {
		if (lista.size() == MAX_STADIUM) {
			throw new StadiumLimitException("Cinema out");
		}
	}

	public static void validateFolio(ArrayList<String> lista, String folio) throws StoleException {

		if (lista.contains(folio)) {
			throw new StoleException("Fucking stole");
		}
	}

}
