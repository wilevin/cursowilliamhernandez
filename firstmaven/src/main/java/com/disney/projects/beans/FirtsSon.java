package com.disney.projects.beans;

public class FirtsSon extends Father {

	public FirtsSon(String name, int age) {
		super(name,age);
	}
	
	@Override
	public void makeArmchair() {

		System.out.println("make some good Armchairs");
	}

	@Override
	public String toString() {
		return "FirtsSon [name=" + getName() + ", age=" + getAge()+"]";
	}
	
	
}
