package com.disney.projects.beans;

import com.disney.projects.interfaces.Figurables;

public class Rectangle extends Figure implements Figurables {

	private static final int TWO = 2;

	private double base;
	private double height;

	public Rectangle(double base, double height) {
		this.base = base;
		this.height = height;
	}

	public Rectangle() {
	}

	public void calculateArea() {
		this.setArea(base * height);
	}

	public void calculatePerimeter() {
		this.setPerimeter(TWO * (base + height));
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
