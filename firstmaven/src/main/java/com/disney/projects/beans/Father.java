package com.disney.projects.beans;

import com.disney.projects.interfaces.Carpenter;

public class Father implements Carpenter {

	private String name;

	private int age;

	public Father() {
	}

	public Father(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void makeArmchair() {
		System.out.println("make a good sofa");
	}

	@Override
	public String toString() {
		return "Father [name=" + name + ", age=" + age + "]";
	}

	@Override
	public boolean equals(Object obj) {

		if (obj != null && obj instanceof Father) {
			Father other = ((Father) obj);
			return other.age == this.age && other.name.equals(this.name);
		}

		return false;
	}

}
