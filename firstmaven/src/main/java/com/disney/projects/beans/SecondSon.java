package com.disney.projects.beans;

public class SecondSon extends Father {

	public SecondSon(String name, int age) {
		super(name, age);
	}

	@Override
	public void makeArmchair() {

		System.out.println("make good wardrobe");
	}

	@Override
	public String toString() {
		return "SecondSon  [name=" + getName() + ", age=" + getAge() + "]";
	}

}
