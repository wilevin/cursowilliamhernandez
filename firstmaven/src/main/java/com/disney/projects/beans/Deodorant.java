package com.disney.projects.beans;

public class Deodorant extends Products {

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	private int stock;

	public Deodorant(int stock, double price, String name) {
		super(stock, price, name);
		this.stock = stock;
	}

	public Deodorant() {
	}

	@Override
	public int calculateStock(int take) {
		return stock - take;
	}

}
