package com.disney.projects.beans;

public abstract class Products {

	private int stock;
	private double price;
	private String name;

	public Products(int stock, double price, String name) {
		this.stock = stock;
		this.price = price;
		this.name = name;
	}

	public Products() {
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public abstract int calculateStock(int take);

	@Override
	public String toString() {
		return "Products [stock=" + stock + ", price=" + price + ", name=" + name + "]";
	}

}
