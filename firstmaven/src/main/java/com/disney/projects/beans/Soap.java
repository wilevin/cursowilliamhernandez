package com.disney.projects.beans;

public class Soap extends Products {

	private int stock;

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Soap() {

	}

	public Soap(int stock, double price, String name) {
		super(stock, price, name);
		this.stock = stock;
	}

	@Override
	public int calculateStock(int take) {
		return stock - take;
	}

}
