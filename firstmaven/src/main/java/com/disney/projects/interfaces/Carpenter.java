package com.disney.projects.interfaces;

public interface Carpenter {
	void makeArmchair();
}
