package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HierachyArrays {

	private static Scanner in;

	public static void main(String[] args) {

		in = new Scanner(System.in);

		List<String> nameChild = new ArrayList<String>();

		System.out.println("number of children?");
		int numberChildren = in.nextInt();

		for (int i = 0; i < numberChildren; i++) {
			System.out.println("name of child?");
			nameChild.add(in.next().toLowerCase());
		}

		System.out.println("Number of children: " + nameChild.size());

		for (String children : nameChild) {
			System.out.println("name: " + children);
		}

	}

}
