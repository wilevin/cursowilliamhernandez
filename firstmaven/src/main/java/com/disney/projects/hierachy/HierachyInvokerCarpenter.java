package com.disney.projects.hierachy;

import java.util.List;
import java.util.ArrayList;

import com.disney.projects.beans.Father;
import com.disney.projects.beans.FirtsSon;
import com.disney.projects.beans.SecondSon;

public class HierachyInvokerCarpenter {

	public static void main(String[] args) {

		Father father = new Father("will", 28);
		Father fatherTwo = new Father("will", 28);

		System.out.println("Name of father: " + father.getName());
		System.out.println("Age of father: " + father.getAge());

		father.makeArmchair();

		FirtsSon firtsSon = new FirtsSon("Cristobal", 8);
		SecondSon secondSon = new SecondSon("Ana", 15);

		firtsSon.makeArmchair();
		System.out.println("name of firts child:" + firtsSon.getName());
		System.out.println("age of firts child:" + firtsSon.getAge());

		secondSon.makeArmchair();
		System.out.println("name of seconf child:" + secondSon.getName());
		System.out.println("age of second child:" + secondSon.getAge());

		System.out.println(father);
		System.out.println(firtsSon);
		System.out.println(secondSon);

		List<Father> parents = new ArrayList<Father>();
		parents.add(father);
		parents.add(firtsSon);
		parents.add(secondSon);

		System.out.println(parents);

		List<String> names = new ArrayList<String>();

		names.add("Juan");
		names.add("Felipe");
		names.add("Gustavo");
		names.add("Ale");
		names.add("Miguelon");

		System.out.println(names);

		names.remove("Felipe");
		System.out.println(names);

		if (father.equals(fatherTwo)) {
			System.out.println("are equals");
		} else {
			System.out.println("are different");
		}

		parents.remove(father);

		System.out.println(parents);

	}

}
