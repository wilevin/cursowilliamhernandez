package com.disney.projects.hierachy;

import com.disney.projects.beans.Rectangle;

public class HierachyInvoker {

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle(2, 4);

		rectangle.calculatePerimeter();
		rectangle.calculateArea();

		System.out.println("The Area of rectangle is: " + rectangle.getArea());
		System.out.println("The Perimeter of rectangle is: " + rectangle.getPerimeter());

	}

}
