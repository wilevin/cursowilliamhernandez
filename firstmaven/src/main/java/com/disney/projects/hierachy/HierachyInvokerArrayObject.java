package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.disney.projects.beans.Father;
import com.disney.projects.beans.FirtsSon;
import com.disney.projects.beans.SecondSon;

public class HierachyInvokerArrayObject {

	private static Scanner in;
	private static List<Father> carpenterList = new ArrayList<Father>();

	public static void main(String[] args) {

		Father father = new Father("Angel", 22);
		FirtsSon firtsSon = new FirtsSon("Angela", 25);
		SecondSon secondSon = new SecondSon("Angelito", 32);

		carpenterList.add(father);
		carpenterList.add(firtsSon);
		carpenterList.add(secondSon);

		in = new Scanner(System.in);

		System.out.println("do you want add a carpenter (1), delete carpenter (2)?");
		int value = in.nextInt();

		switch (value) {
		case 1:
			System.out.println("enter the type of carpenter Father (1)  FirtsSon (2) SecondSon (3): ");
			int type = in.nextInt();

			System.out.println("number of carpenters");
			int numberCarpenter = in.nextInt();

			addCarpenter(numberCarpenter, type);

			System.out.println(carpenterList);

			break;

		case 2:
			System.out.println(carpenterList);
			System.out.println("enter the type of carpenter want delete Father (1)  FirtsSon (2) SecondSon (3): ");
			int type1 = in.nextInt();

			if (type1 == 1) {
				deleteCarpenter(father);
			} else if (type1 == 2) {
				deleteCarpenter(firtsSon);
			} else if (type1 == 3) {
				deleteCarpenter(secondSon);
			}
			System.out.println(carpenterList);

			break;
		default:
			System.out.println("number invalid");
		}

	}

	public static void addCarpenter(int number, int type) {

		for (int i = 0; i < number; i++) {

			System.out.println("name of carpenter");
			String name = in.next().toLowerCase();

			System.out.println("age of carpenter");
			int age = in.nextInt();

			if (type == 1) {
				carpenterList.add(new Father(name, age));
			} else if (type == 2) {
				carpenterList.add(new FirtsSon(name, age));
			} else if (type == 3) {
				carpenterList.add(new SecondSon(name, age));
			}

		}

	}

	public static void deleteCarpenter(Father father) {
		carpenterList.remove(father);
	}

}