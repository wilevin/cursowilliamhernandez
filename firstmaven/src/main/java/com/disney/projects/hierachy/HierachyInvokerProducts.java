package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.disney.projects.beans.Deodorant;
import com.disney.projects.beans.Products;
import com.disney.projects.beans.Soap;
import com.disney.projects.operations.impl.StoreOperationImpl;

public class HierachyInvokerProducts {

	private static final Scanner in = new Scanner(System.in);
	private static List<Products> productsList = new ArrayList<Products>();
	private static int cantZote = 0;
	private static int cantAxe = 0;
	private static int cantAriel = 0;
	private static double valueTotal = 0;

	public static void main(String[] args) {

		StoreOperationImpl operation = new StoreOperationImpl();

		Soap soap = new Soap(1000, 300, "zote");
		Soap ariel = new Soap(1000, 900, "ariel");
		Deodorant deodorant = new Deodorant(200, 55, "AXE");

		productsList.add(soap);
		productsList.add(deodorant);
		productsList.add(ariel);

		operation.listProducts(productsList);

		String opc = "";
		double cont = 0;

		do {

			System.out.println("enter 1(zote) 2(AXE) 3(Ariel): ");
			int value = in.nextInt();

			switch (value) {
			case 1:

				System.out.println("enter number of product: ");
				int numberZote = in.nextInt();

				if (soap.calculateStock(numberZote) >= 0) {
					System.out.println("your shop: " + operation.shoppingCarts(soap, numberZote));
					cantZote += numberZote;
					valueTotal += operation.calculateDiscount(operation.calculateTotal(numberZote, soap.getPrice()));

				} else {
					System.out.println("non-existing more product try new");
				}
				cont += operation.calculateDiscount(operation.calculateTotal(numberZote, soap.getPrice()));

				break;
			case 2:

				System.out.println("enter number of product: ");
				int numberAxe = in.nextInt();

				if (soap.calculateStock(numberAxe) >= 0) {
					System.out.println("your shop: " + operation.shoppingCarts(deodorant, numberAxe));
					cantAxe += numberAxe;
					valueTotal += operation
							.calculateDiscount(operation.calculateTotal(numberAxe, deodorant.getPrice()));
				} else {
					System.out.println("non-existing more product try new");
				}
				cont += operation.calculateDiscount(operation.calculateTotal(numberAxe, deodorant.getPrice()));

				break;

			case 3:
				System.out.println("enter number of product: ");
				int numberAriel = in.nextInt();

				if (soap.calculateStock(numberAriel) >= 0) {
					System.out.println("your shop: " + operation.shoppingCarts(soap, numberAriel));
					cantAriel += numberAriel;
					valueTotal += operation
							.calculateDiscount(operation.calculateTotal(numberAriel, soap.getPrice()));
				} else {
					System.out.println("non-existing more product try new");
				}
				cont += operation.calculateDiscount(operation.calculateTotal(numberAriel, soap.getPrice()));

				break;
			default:
				System.out.println("number invalid");
			}

			System.out.println("new operation (s) or (n)? ");
			opc = in.next().toLowerCase();
		} while (opc.equals("s"));
		System.out.println("Name------------ quatity");
		System.out.println("zote------------ " + cantZote);
		System.out.println("Axe------------ " + cantAxe);
		System.out.println("Ariel------------ " + cantAriel);
		System.out.println("Value total................. " + valueTotal);
	}

}
