package com.disney.projects.operations;

import com.disney.projects.beans.Products;

public interface Operation {

	void calculateChage(int money);

	int calculateThousands(int res);

	int calculateHundreds(int res);

	int calculateTens(int res);

	int calculateUnits(int res);

	Products typeProduct(int type);

}
