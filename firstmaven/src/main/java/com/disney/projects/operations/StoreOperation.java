package com.disney.projects.operations;

import java.util.List;

import com.disney.projects.beans.Products;

public interface StoreOperation {

	double calculateDiscount(double totalValue);

	void listProducts(List<Products> list);

	void deleteProduct(Products product, List<Products> productsList);

	double calculateTotal(int cuantity, double price);
	
	List<Products> shoppingCarts(Products obj,int quantity);
 	
}
