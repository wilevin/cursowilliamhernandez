package com.disney.projects.operations.impl;

import com.disney.projects.beans.Deodorant;
import com.disney.projects.beans.Products;
import com.disney.projects.beans.Soap;
import com.disney.projects.operations.Operation;

public class OperationImpl implements Operation {

	private static final int THOUSAND = 1000;
	private static final int HUNDRED = 100;
	private static final int TEN = 10;
	private static final int UNITS = 1;

	public void calculateChage(int money) {

		System.out.println("change ticket of $1000: <------------> " + calculateThousands(money));
		System.out.println("change ticket of $100: <-------------> " + calculateHundreds(money));
		System.out.println("change currey of $10: <--------------> " + calculateTens(money));
		System.out.println("change currey of $1: <--------------> " + calculateUnits(money));

	}

	public int calculateThousands(int res) {

		return res / THOUSAND;
	}

	public int calculateHundreds(int res) {

		return (res % THOUSAND) / HUNDRED;
	}

	public int calculateTens(int res) {

		return ((res % THOUSAND) % HUNDRED) / TEN;
	}

	public int calculateUnits(int res) {

		return (((res % THOUSAND) % HUNDRED) % TEN) / UNITS;
	}

	public Products typeProduct(int type) {

		Products product = new Soap(); 
		
		if (type == 1) {
			return product = new Soap();
		} else if (type == 2) {
			return product = new Deodorant();
		} else if (type == 3) {
			return product = new Soap();
		}

		return product;
	}

}
