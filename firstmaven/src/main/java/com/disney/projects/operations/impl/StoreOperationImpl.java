package com.disney.projects.operations.impl;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.beans.Products;
import com.disney.projects.operations.StoreOperation;

public class StoreOperationImpl implements StoreOperation {

	private static final int TWO_THOUSAND = 2000;
	private static final int THREE_THOUSAND = 3000;
	private static final double THIRTY_DISCOUNT = .30;
	private static final double FIFTY_DISCOUNT = .50;
	private static List<Products> productList = new ArrayList<Products>();

	public double calculateDiscount(double totalQuantity) {

		double discountTotal = totalQuantity;

		if (totalQuantity >= TWO_THOUSAND && totalQuantity < THREE_THOUSAND) {
			discountTotal = totalQuantity - (totalQuantity * THIRTY_DISCOUNT);
		} else if (totalQuantity >= THREE_THOUSAND) {
			discountTotal = totalQuantity - (totalQuantity * FIFTY_DISCOUNT);
		}

		return discountTotal;

	}

	public void listProducts(List<Products> list) {

		for (Products products : list) {

			System.out.println("name of product: " + products.getName() + " price: $" + products.getPrice());
		}

	}

	public void deleteProduct(Products product, List<Products> productsList) {
		productsList.remove(product);
	}

	public double calculateTotal(int cuantity, double price) {

		return price * cuantity;
	}

	public List<Products> shoppingCarts(Products obj, int quantity) {

		for (int i = 0; i < quantity; i++) {
			productList.add(obj);
		}

		return productList;
	}

}
