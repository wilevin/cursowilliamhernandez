package com.disney.projects.runatm;

import java.util.Scanner;

import com.disney.projects.operations.impl.OperationImpl;

public class InvokerAtm {

	private static final Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		OperationImpl operation = new OperationImpl();
		String opc="";
		do {

			System.out.println("enter amount of money:");
			int money = in.nextInt();
			operation.calculateChage(money);
			
			System.out.println("new operation (s) or (n)? ");
			opc = in.next().toLowerCase();
			
		} while (opc.equals("s"));
		
		

	}

}
