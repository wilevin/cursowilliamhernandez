package com.disney.projects.abstracct;

public abstract class Products {

	private String name;
	private int price;

	public Products() {
	}
	
	public Products(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public void characters() {

		System.out.println("article name: " + name);
		System.out.println("price " + price);
		calculeteDiscount();
	}

	public abstract void calculeteDiscount();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
