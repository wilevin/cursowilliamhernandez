package com.disney.projects.abstracct;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class MapsInvoker {

	private static Map<Integer, Products> mapProducts = new HashMap<Integer, Products>();
	private static Scanner in = new Scanner(System.in);
	
	private static final int ONE=1;
	private static final int TWO =2;
	private static final int THREE=3;
	
	public static void main(String[] args) {

		fillInMap();

		String opc = "";
		int key = 3;
		do {

			System.out.println("choose the operation add (1) delete (2) view Products(3) ?");
			int operation = in.nextInt();
			key++;
			switch (operation) {
			case 1:
				System.out.println("choose type of product soccer(1) baseball(2) tranning (3) ?");
				int type = in.nextInt();

				addProduct(type, key);

				break;
			case 2:
				System.out.println("choose key of product");
				int type2 = in.nextInt();

				deleteProduct(type2);
				break;
			case 3:
				Iterator<Integer> it = mapProducts.keySet().iterator();
				while (it.hasNext()) {
					Integer key3 = (Integer) it.next();
					System.out.println("Clave: " + key3 + " -> Valor: " + mapProducts.get(key3).getName());
				}

				break;
			default:
				break;
			}

			System.out.println("new operation (yes) or (no)?");
			opc = in.next().toLowerCase();

		} while (opc.equals("yes"));

	}

	public static void fillInMap() {
		mapProducts.put(1, new Soccer("Shoes", 123));
		mapProducts.put(2, new Baseball("bat", 223));
		mapProducts.put(3, new Tranning("t-shirt", 23));
	}

	public static void addProduct(int type, int key) {

		System.out.println("name of product? ");
		String name = in.next();
		System.out.println("price of product");
		int price = in.nextInt();

		if (type == ONE) {
			mapProducts.put(key, new Soccer(name, price));
		} else if (type == TWO) {
			mapProducts.put(key, new Baseball(name, price));
		} else if (type == THREE) {
			mapProducts.put(key, new Tranning(name, price));
		}

	}

	public static void deleteProduct(int key) {
		mapProducts.remove(key);
	}

}
