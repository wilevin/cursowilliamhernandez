package com.disney.projects.abstracct;

import java.util.ArrayList;
import java.util.List;

public class InvokeClassAbstract {

	public static void main(String[] args) {

		Soccer soccer = new Soccer("shoes", 345);
		Baseball baseball = new Baseball("bat", 150);
		Tranning tranning = new Tranning("t-shirt", 100);
		
		
		List<Products> products = new ArrayList<Products>();
		
		products.add(soccer);
		products.add(baseball);
		products.add(tranning);
		
		for (Products product : products) {
			product.characters();
			System.out.println("-------------------------------------------------");
		}
		
	}

}
